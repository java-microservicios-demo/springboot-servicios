package com.ejemplo.springboot.app.oauth.models;

import java.io.Serializable;

public class Role implements Serializable {

	private static final long serialVersionUID = 6713576710234934563L;
	private Long id;
	private String nombre;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
