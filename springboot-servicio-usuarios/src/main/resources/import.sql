INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('jesus','$2a$10$yCCyn4L65uBjWxDg0tRA6.dn28z6YROS9bTXz1TXFNHuIN8e3Oba2',1, 'Jesus', 'Azabache','jesus@gmail.com');
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('javier','$2a$10$fw/HzmvEupnbqeu6GCpRCurB51Y6AiGr13F7QbjXKNBbAWncA.I22',1, 'Javier', 'Guzman','javier@gmail.com');
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('andres','$2a$10$fw/HzmvEupnbqeu6GCpRCurB51Y6AiGr13F7QbjXKNBbAWncA.I22',1, 'Andres', 'Caceres','andres@gmail.com');

INSERT INTO `roles` (nombre) VALUES ('ROLE_USER');
INSERT INTO `roles` (nombre) VALUES ('ROLE_ADMIN');

INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (1, 1);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (1, 2);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (2, 1);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (3, 1);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (3, 2);

INSERT INTO `notas` (curso, nota, usuario_id) VALUES ('Matematica', '17',1);
INSERT INTO `notas` (curso, nota, usuario_id) VALUES ('Lenguaje', '15',1);
INSERT INTO `notas` (curso, nota, usuario_id) VALUES ('Fisica','16',1);

INSERT INTO `notas` (curso, nota, usuario_id) VALUES ('Matematica', '10',2);
INSERT INTO `notas` (curso, nota, usuario_id) VALUES ('Lenguaje', '12',2);
INSERT INTO `notas` (curso, nota, usuario_id) VALUES ('Fisica','13',2);

INSERT INTO `notas` (curso, nota, usuario_id) VALUES ('Matematica', '05',3);
INSERT INTO `notas` (curso, nota, usuario_id) VALUES ('Lenguaje', '09',3);
INSERT INTO `notas` (curso, nota, usuario_id) VALUES ('Fisica','07',3);
