package com.ejemplo.springboot.app.usuarios.models.dto;

import java.util.List;

public class UsuarioDto {

	private Long id;
	private String username;
	private Boolean enabled;
	private List<RoleDto> roles;
	private List<NotaDto> notas;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	public List<RoleDto> getRoles() {
		return roles;
	}
	public void setRoles(List<RoleDto> roles) {
		this.roles = roles;
	}
	public List<NotaDto> getNotas() {
		return notas;
	}
	public void setNotas(List<NotaDto> notas) {
		this.notas = notas;
	}
}
