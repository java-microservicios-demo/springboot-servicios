package com.ejemplo.springboot.app.usuarios.services;

import com.ejemplo.springboot.app.usuarios.models.entity.Nota;
import com.ejemplo.springboot.app.usuarios.models.response.NotaResponse;

public interface NotaService {

	public NotaResponse guardarNotaUsuario(Nota nota);
}
