package com.ejemplo.springboot.app.usuarios.services;

import java.util.List;

import com.ejemplo.springboot.app.usuarios.models.entity.Usuario;
import com.ejemplo.springboot.app.usuarios.models.response.UsuarioResponse;

public interface UsuarioService {

	public List<Usuario> findAll();
	public UsuarioResponse findByUsername(String username);
}
