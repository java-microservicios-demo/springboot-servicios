package com.ejemplo.springboot.app.usuarios.services.impl;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ejemplo.springboot.app.usuarios.models.dao.UsuarioDao;
import com.ejemplo.springboot.app.usuarios.models.entity.Usuario;
import com.ejemplo.springboot.app.usuarios.models.response.UsuarioResponse;
import com.ejemplo.springboot.app.usuarios.services.UsuarioService;
import com.ejemplo.springboot.app.usuarios.util.MapperUtil;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioDao usuarioDao;
	
	@Override
	public List<Usuario> findAll() {
		return (List<Usuario>) usuarioDao.findAll();
	}
	
	@Override
	public UsuarioResponse findByUsername(String username) {
		Usuario usuario = usuarioDao.obtenerPorUsername(username);
		String jsonObj = MapperUtil.objectToJson(usuario);
		UsuarioResponse userObj = MapperUtil.fromJson(jsonObj, UsuarioResponse.class);
		return userObj;
	}
}
